var class_medallion_control =
[
    [ "SetSpike", "class_medallion_control.html#a65eb2983d973f62afbfec3cf92733433", null ],
    [ "ToggleSpikes", "class_medallion_control.html#a3f63fe19b2ba62be73ea975afbc8f4be", null ],
    [ "DownMats", "class_medallion_control.html#a18ad3ecae2ad06e27bd394d530c4eb1b", null ],
    [ "DownSpike", "class_medallion_control.html#a7d518cb38da96effdff5c3a0450f294e", null ],
    [ "LeftMats", "class_medallion_control.html#a3e8a743ae7a703bac6eb63367e8a8d8d", null ],
    [ "LeftSpike", "class_medallion_control.html#ad5034d0c41e81ba6950e0dcd0c61bce8", null ],
    [ "RightMats", "class_medallion_control.html#a1c743cffbf1c86c51d30066e867ae464", null ],
    [ "RightSpike", "class_medallion_control.html#a38a2238e65000d38c33e8ebb40ad5b1c", null ],
    [ "UpMats", "class_medallion_control.html#addf964f8b21c39fcc72bd1b5572388a6", null ],
    [ "UpSpike", "class_medallion_control.html#a3ce08619505c7c17f0e8bd762168a9aa", null ]
];
var class_upgrade_manager =
[
    [ "Convert", "class_upgrade_manager.html#a6f2ae92a405e6f8d85ccff0c4d5f7783", null ],
    [ "Convert", "class_upgrade_manager.html#a48a0d50e85f22c30b5a7436c7d649408", null ],
    [ "GetBodyBagsUpgrades", "class_upgrade_manager.html#a83e85066f6b84234378229539bdae5a0", null ],
    [ "GetBoltCuttersUpgrades", "class_upgrade_manager.html#aecd5407174be3d14fe37025b9ae9fefd", null ],
    [ "GetCurrentBodyBag", "class_upgrade_manager.html#a4310af36b0d04dd2d35615c9cd2929ac", null ],
    [ "GetCurrentBolltCutters", "class_upgrade_manager.html#add748c93ae6903a107661f23fd414bda", null ],
    [ "GetCurrentShoes", "class_upgrade_manager.html#af0d243eab40ef16681cda7e18367ce43", null ],
    [ "GetCurrentShovel", "class_upgrade_manager.html#a344e78fbeb73a4ad119febe603a5780b", null ],
    [ "GetShoesUpgrades", "class_upgrade_manager.html#ad55538552884c9d76e05c730e38580b4", null ],
    [ "GetShovelUpgrades", "class_upgrade_manager.html#ad7b9edf00b9e970af51c1b72c5d24a2f", null ],
    [ "UpgradeBodyBag", "class_upgrade_manager.html#ace07916b537615eeb092432d31b9bb81", null ],
    [ "UpgradeBoltCutter", "class_upgrade_manager.html#ae480ebe9e6b219adb20e0b410bc69cc7", null ],
    [ "UpgradeShoes", "class_upgrade_manager.html#aebf049454001984ca3ac1fe79c851d2d", null ],
    [ "UpgradeShovel", "class_upgrade_manager.html#a366539e3c1c5a211c0ec5cac77ccbd58", null ]
];
var class_coffin_cage_mini_game =
[
    [ "CheckWin", "d1/df9/class_coffin_cage_mini_game.html#a902e65a1ebb1e37c9349f4a07705ee2a", null ],
    [ "CutTrap", "d1/df9/class_coffin_cage_mini_game.html#a7382945749a8ed4f7de50b938c64c3f1", null ],
    [ "GetCurrentNumberOfCuts", "d1/df9/class_coffin_cage_mini_game.html#a9e4e08e8ef3dbc6890117d3cdfb9c33f", null ],
    [ "IncreaseDificulty", "d1/df9/class_coffin_cage_mini_game.html#a5c9630c311a3831bc1fd87701ffebd75", null ],
    [ "SetCutNumber", "d1/df9/class_coffin_cage_mini_game.html#ad115572b9e240d9c68aad0826e33b6d5", null ],
    [ "ButtonSprite", "d1/df9/class_coffin_cage_mini_game.html#a0e3ea1a7e5f78101b2c1d33565c9527f", null ],
    [ "PossiblePos", "d1/df9/class_coffin_cage_mini_game.html#a82e5baa343821a22c5b0e5f720377215", null ]
];
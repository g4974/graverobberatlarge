var _enums_8cs =
[
    [ "Direction", "_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aa", [
      [ "UP", "_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaafbaedde498cdead4f2780217646e9ba1", null ],
      [ "LEFT", "_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaa684d325a7303f52e64011467ff5c5758", null ],
      [ "RIGHT", "_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaa21507b40c80068eda19865706fdc2403", null ],
      [ "DOWN", "_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaac4e0e4e3118472beeb2ae75827450f1f", null ]
    ] ],
    [ "GraveQuality", "_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54", [
      [ "LOW", "_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54a41bc94cbd8eebea13ce0491b2ac11b88", null ],
      [ "MEDIUM", "_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54ac87f3be66ffc3c0d4249f1c2cc5f3cce", null ],
      [ "HIGH", "_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54ab89de3b4b81c4facfac906edf29aec8c", null ],
      [ "NULL", "_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54a6c3e226b4d4795d518ab341b0824ec29", null ]
    ] ],
    [ "GraveyardTier", "_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25c", [
      [ "LOW", "_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25ca41bc94cbd8eebea13ce0491b2ac11b88", null ],
      [ "MEDIUM", "_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25cac87f3be66ffc3c0d4249f1c2cc5f3cce", null ],
      [ "HIGH", "_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25cab89de3b4b81c4facfac906edf29aec8c", null ],
      [ "TUTORIAL", "_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25cab8e2e6f28d96c901009d75312887a6da", null ]
    ] ],
    [ "GuardState", "_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89", [
      [ "PATROL", "_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89a5383e862b017e18e1087d61e9f2684d9", null ],
      [ "ALERT", "_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89a320f86f60f25459ba5550e000b2c3929", null ],
      [ "CAUGHT", "_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89af67a3bdb4927fa4ed55d0a24685ac4b3", null ]
    ] ],
    [ "ImageElement", "_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517", [
      [ "RED", "_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517aa2d9547b5d3dd9f05984475f7c926da0", null ],
      [ "GREEN", "_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517a9de0e5dd94e861317e74964bed179fa0", null ],
      [ "BLUE", "_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517a1b3e1ee9bff86431dea6b181365ba65f", null ],
      [ "ALPHA", "_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517a002101f8725e5c78d9f30d87f3fa4c87", null ]
    ] ],
    [ "TileType", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1", [
      [ "GRASS", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1aa3030e42cd8f04255711905a9182399f", null ],
      [ "GRAVE", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a0454988b6250b7e13d8e9222e118c127", null ],
      [ "PATH", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a5ffb5f0d0de78321df46fc7c93ca64a3", null ],
      [ "FENCE", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a680f61224afe352a026a616896f47a29", null ],
      [ "BUSH", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a837b47a7cfe2c08329230f73f9e2a241", null ],
      [ "TREE", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1ac0f8e24cf041639c4fc457ebec9490eb", null ],
      [ "MAUSOLEUM", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1aa94abe5bd38a18a47b94c018915022df", null ],
      [ "WOODFENCE", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1ac46be538ef00967c7f5e12e081a05e8e", null ],
      [ "ROCK", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1aafeb717aa2a101f7f64840e0be38c171", null ],
      [ "BIRDBATH", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1afb750cd9ecbcb666bea36728e337966b", null ],
      [ "CLOUD", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1aa51b1ad8bfa5207b7eb5c2ca7bfc5a3c", null ],
      [ "PILLAR", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a0fb3a7dfd4d830004a0d9a6f8b9469b3", null ],
      [ "GRAVELEFT", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a10231ad1a033eca8b0d26bb45bb99df8", null ],
      [ "GRAVERIGHT", "_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a5c407100388acb8ef7d4a9276715177c", null ]
    ] ]
];
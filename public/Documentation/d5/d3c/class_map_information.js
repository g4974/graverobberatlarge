var class_map_information =
[
    [ "GetBushMinDistance", "d5/d3c/class_map_information.html#adc779f662ff2e98181b799b64239ac7c", null ],
    [ "GetDetachedGraveChance", "d5/d3c/class_map_information.html#a60f0545d1ada50ff70b01c7d829b6d59", null ],
    [ "GetGraveClusters", "d5/d3c/class_map_information.html#a5b678f25ebf859fc03ac322d253b7900", null ],
    [ "GetGraveDensity", "d5/d3c/class_map_information.html#a8e379ad94595396e66d5ea10e89fe58b", null ],
    [ "GetGraveDirection", "d5/d3c/class_map_information.html#a30bcfde01ee40a126c4486433c5a0a29", null ],
    [ "GetGraveRNGIncrease", "d5/d3c/class_map_information.html#a50254fb869dd3ed1da1a7544d0f82594", null ],
    [ "GetGraveyardEntrancePosition", "d5/d3c/class_map_information.html#a87c1c1448b3fe8dd44f709f857bec488", null ],
    [ "GetGuardNumber", "d5/d3c/class_map_information.html#a68007336e77eb7b1301f98186070b047", null ],
    [ "GetLevelTier", "d5/d3c/class_map_information.html#a593d03dad519a7d210efa192a50916ce", null ],
    [ "GetMapColor", "d5/d3c/class_map_information.html#a864d462be77ec97308d1fa137db48700", null ],
    [ "GetMapImage", "d5/d3c/class_map_information.html#a0bd050323b1a32138bce48943d1a1f6b", null ],
    [ "GetMaxBushes", "d5/d3c/class_map_information.html#ad4eda208c05ce484251eb44bf1fb259c", null ],
    [ "GetMaxTrees", "d5/d3c/class_map_information.html#afa2c94cb3e2aaba9f96bcf4a489ffdf6", null ],
    [ "GetPrebuilt", "d5/d3c/class_map_information.html#a75501a5dc3f867154387c39815bb96eb", null ],
    [ "GetSpawnProtectionDistance", "d5/d3c/class_map_information.html#a393b637b53d02d840a248bfefd7c3545", null ],
    [ "GetTreeMinDistance", "d5/d3c/class_map_information.html#a8026e307307546d2044930b5893932bc", null ]
];
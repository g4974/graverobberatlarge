var class_quests =
[
    [ "Quests", "class_quests.html#afb334822ec57915b78bdb254f7a9e6d0", null ],
    [ "Evaluate", "class_quests.html#afad6515ecf503b14a0a1532fa4a91cd7", null ],
    [ "GetActive", "class_quests.html#a72a40a1ddd10f0706ad78d06d1931192", null ],
    [ "GetBodyQuality", "class_quests.html#aeffdfeffa95e979b76da141d26d1e80e", null ],
    [ "GetCurrentAmount", "class_quests.html#ab33cb9d99697f38182674c67568bfa0b", null ],
    [ "GetDescription", "class_quests.html#ad0738dc6810034f402639661ef029948", null ],
    [ "GetPrimType", "class_quests.html#a0db34317da8160fd263eebbcb0ffff86", null ],
    [ "GetRequiredAmount", "class_quests.html#a2b148143e147a27d636fe9f6b27f00b2", null ],
    [ "GetReward", "class_quests.html#a40c6eebeb640111bd0a4690b14d685e6", null ],
    [ "GetTag", "class_quests.html#ae2b09c686706b40db3fea563b7d16ca7", null ],
    [ "GetTitle", "class_quests.html#a646bd485d5d40e4c6aa6f0996c7cb0b2", null ],
    [ "SetActive", "class_quests.html#aa6ecc9a093264369bbcb2564c769d3ff", null ],
    [ "SetCurrentAmount", "class_quests.html#a3890c91f7969b4d3d710d67ab7502acb", null ],
    [ "SetDescription", "class_quests.html#a7b362b4d22b9204da4608fda930f0fc7", null ],
    [ "SetRequiredAmount", "class_quests.html#a392d962fe8a5c884516c33633a3355fa", null ],
    [ "SetReward", "class_quests.html#a9fc37189594fde754dfeee2ffe1458b0", null ],
    [ "SetTitle", "class_quests.html#af0703120b0f0d8abdb175a0f97979c3a", null ]
];
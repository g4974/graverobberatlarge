var _body_8cs =
[
    [ "Body", "class_body.html", "class_body" ],
    [ "BodyQuality", "_body_8cs.html#a5daecede1d758df2cfe6bb5720bab7e4", [
      [ "Skeleton", "_body_8cs.html#a5daecede1d758df2cfe6bb5720bab7e4a6ab48f7ed56efc362f41853c5616bf75", null ],
      [ "Decayed", "_body_8cs.html#a5daecede1d758df2cfe6bb5720bab7e4ad2eecada55d53b622609b46b4ba2b2a8", null ],
      [ "SlightDecay", "_body_8cs.html#a5daecede1d758df2cfe6bb5720bab7e4a2dc6731046c4e9b33844382ca4c3842c", null ],
      [ "Fresh", "_body_8cs.html#a5daecede1d758df2cfe6bb5720bab7e4afe8965a9748c1bc48af1711be837124e", null ]
    ] ]
];
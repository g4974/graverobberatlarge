var class_game_manager =
[
    [ "AddBody", "db/d10/class_game_manager.html#aaae83c96c1ffed3f53283178d37195b6", null ],
    [ "ChangeScene", "db/d10/class_game_manager.html#aba8967781cc5100e8dcbd6affeb89dfe", null ],
    [ "ChangeScene", "db/d10/class_game_manager.html#a80c07c0f5f5769b1fde49e41577cdbbe", null ],
    [ "CompleteTask", "db/d10/class_game_manager.html#a71863b0b355f3ade437c7dd64fff4c49", null ],
    [ "CreateQuest", "db/d10/class_game_manager.html#ac681e0723423ffcdd52b64404d1b63ab", null ],
    [ "CreateQuest", "db/d10/class_game_manager.html#aae47f2a376980fca502051f9c63407eb", null ],
    [ "EndRun", "db/d10/class_game_manager.html#a2d951a59e3dcadf8b14e4ce6c37f061b", null ],
    [ "ExitGame", "db/d10/class_game_manager.html#a8cec87903cc4b66f1fdee1967e86ba51", null ],
    [ "GetBodyCollection", "db/d10/class_game_manager.html#a73ddb1f69404b9fffdde52a186533302", null ],
    [ "GetGravesCompleted", "db/d10/class_game_manager.html#a9613113b24a090171e1bb13402d97144", null ],
    [ "GetMoney", "db/d10/class_game_manager.html#ab5da814aae54d4bceb91cac7f7290b4b", null ],
    [ "GetMoneyCap", "db/d10/class_game_manager.html#ae9c1d96d5383443af62010d365ba1791", null ],
    [ "GetTreasureCollection", "db/d10/class_game_manager.html#a6de115ce968a13d0a7c2c50078516e07", null ],
    [ "IsHidden", "db/d10/class_game_manager.html#a8e5c7aa15c8df6715e4d399cdd1a11d5", null ],
    [ "MergeBodies", "db/d10/class_game_manager.html#a50e2a15e87d37c37784d563682dd9d82", null ],
    [ "QuestTurnInOff", "db/d10/class_game_manager.html#a320b6271adeac88f68384fd40245f430", null ],
    [ "QuestTurnInOn", "db/d10/class_game_manager.html#a9a421f84b98c213fa73e5f197b8d5cd8", null ],
    [ "RetunrTreasure", "db/d10/class_game_manager.html#a62acb895195ce3a5be9210d4607fb571", null ],
    [ "ReturnBodyList", "db/d10/class_game_manager.html#a1c329748a7608b5b0c38a8f1dce8316f", null ],
    [ "SellBody", "db/d10/class_game_manager.html#a20675f5bb22553e75651bc54c35f5fcd", null ],
    [ "SellTreasure", "db/d10/class_game_manager.html#a7b0a56b28c5cee2f08ff83f43f6efb79", null ],
    [ "SetBodyCollection", "db/d10/class_game_manager.html#adb839f0dcd334f68c2ee9a985853b622", null ],
    [ "SetMoneyCap", "db/d10/class_game_manager.html#a07931523d038b65787e731eeccb432e6", null ],
    [ "SetTreasureCollection", "db/d10/class_game_manager.html#a6ec972e11e60a30167f53514f1858eb0", null ],
    [ "StartRun", "db/d10/class_game_manager.html#aa8a1297f6f46e8373ab85a62955a12da", null ],
    [ "ToggleHidden", "db/d10/class_game_manager.html#a8a9d36e17648bfcc9527c0279a21f04a", null ],
    [ "ToggleMinigame", "db/d10/class_game_manager.html#aeb3e2f35b334ed6ca2bb6a5239b1b8a8", null ],
    [ "UpdateGraveTotal", "db/d10/class_game_manager.html#a9fa748bb989b1f0ba6c8088725f4f4a2", null ],
    [ "UpdateMoney", "db/d10/class_game_manager.html#abb1c222eefb6e7582983de8a59056098", null ],
    [ "_DialogueManager", "db/d10/class_game_manager.html#a627b348bde24c57e570f90a4d86c28cb", null ],
    [ "_QuestManager", "db/d10/class_game_manager.html#a9144056acf63d94bd21a4f8959f15570", null ],
    [ "_RunManager", "db/d10/class_game_manager.html#a70c4906edb2b7a66c483f8998fcd4594", null ],
    [ "_SceneManager", "db/d10/class_game_manager.html#a6fcfaa4e97d16bef0ff7f37e8d35af38", null ],
    [ "_UpgradeManager", "db/d10/class_game_manager.html#a14c581bfcc978f497fef3081307bce23", null ],
    [ "Inventory", "db/d10/class_game_manager.html#aad34417c996852632b41c2e5263cbcaf", null ]
];
var class_dialogue =
[
    [ "AddVar", "class_dialogue.html#aef7ce928be67fc788317f82be836399e", null ],
    [ "GetLineNumber", "class_dialogue.html#ab9feca9972d2d161ea2be62ff6997c28", null ],
    [ "GetRandom", "class_dialogue.html#a6a2bdf53e8d842066e03132b5bf978f4", null ],
    [ "IsEnd", "class_dialogue.html#ae8c95bee410a90a34b078461979ad03e", null ],
    [ "NextLine", "class_dialogue.html#a1c7cb3b4f4ff488cbf3d069ae1a13124", null ],
    [ "NextLineVars", "class_dialogue.html#a858ac0f41e43509987b5800ac5e7d39a", null ],
    [ "OnEnable", "class_dialogue.html#aaf113062befa09d643ebd9a5629ca7d5", null ],
    [ "ResetDialogue", "class_dialogue.html#a8eeaf735355b1740a29711ff6206a9c0", null ],
    [ "SetDialogueUI", "class_dialogue.html#afef8d4afc2a211e77f21471041884a11", null ],
    [ "SetScript", "class_dialogue.html#a69133b8f8cd3875f2d0ddf3acdfb05b1", null ],
    [ "SetScriptVars", "class_dialogue.html#a180ac6da7ce18ec4a85787a10107b963", null ],
    [ "Line", "class_dialogue.html#a050efecaf7656858b41286aa18b3d6fb", null ],
    [ "Portrait", "class_dialogue.html#a93512ce860788b0eec56631a8b4e5df1", null ],
    [ "Title", "class_dialogue.html#a6948aed8b047f26c404e858b5b21ec0c", null ]
];
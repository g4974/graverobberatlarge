var searchData=
[
  ['ignorefrombuild_0',['ignoreFromBuild',['../class_unity_engine_1_1_a_i_1_1_nav_mesh_modifier.html#a43adb4e69509a139de04fadbb98a74c6',1,'UnityEngine::AI::NavMeshModifier']]],
  ['ignorenavmeshagent_1',['ignoreNavMeshAgent',['../class_unity_engine_1_1_a_i_1_1_nav_mesh_surface.html#a38bbd52829348bbcb04c3c369d1437b8',1,'UnityEngine::AI::NavMeshSurface']]],
  ['ignorenavmeshobstacle_2',['ignoreNavMeshObstacle',['../class_unity_engine_1_1_a_i_1_1_nav_mesh_surface.html#ab5f7b99886990c7f0b8677351c4de102',1,'UnityEngine::AI::NavMeshSurface']]],
  ['imageelement_3',['ImageElement',['../_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517',1,'Enums.cs']]],
  ['imageloader_4',['ImageLoader',['../class_image_loader.html',1,'']]],
  ['imageloader_2ecs_5',['ImageLoader.cs',['../_image_loader_8cs.html',1,'']]],
  ['increasedificulty_6',['IncreaseDificulty',['../class_coffin_cage_mini_game.html#a544da514d1bcdfc6359e31c08933fa90',1,'CoffinCageMiniGame.IncreaseDificulty()'],['../class_mini_game.html#a39ed5137934c956438f25c3c8209bfc8',1,'MiniGame.IncreaseDificulty()']]],
  ['initialize_7',['Initialize',['../class_general_tile.html#aae11cc1b2a40156bd9b56cb738882c32',1,'GeneralTile.Initialize()'],['../class_grave_tile.html#a41e3b02c3c6aa55de8597f9920047d3d',1,'GraveTile.Initialize()']]],
  ['initializetile_8',['InitializeTile',['../class_tile_initialize.html#a526bd6fe252d9012be5419b9014d579c',1,'TileInitialize']]],
  ['initiate_9',['Initiate',['../class_grave_info_refactor.html#a816a13d23f4082fe941aeac8eae19158',1,'GraveInfoRefactor']]],
  ['initiategrave_10',['InitiateGrave',['../class_tile_data.html#ab1cf1c22cdcd9169f1150cb2e3c50a00',1,'TileData']]],
  ['instance_11',['Instance',['../class_stealth_manager.html#aeccd15ba07328636f38b630f7da9e3e4',1,'StealthManager.Instance()'],['../class_u_i_manager.html#a006246dc7fcecbe2387d70731191d4f6',1,'UIManager.Instance()'],['../class_game_manager.html#ad3e717f4fb0f378b969f4457de81f23e',1,'GameManager.Instance()']]],
  ['instructtext_12',['InstructText',['../class_trip_wire_mini_game.html#a8355bdb6f128809d3af80911ddfdd8cc',1,'TripWireMiniGame']]],
  ['interact_13',['Interact',['../class_courtyard_collisions.html#a0a10ac82fda3354b53dce911f4b230c5',1,'CourtyardCollisions']]],
  ['interactable_14',['Interactable',['../class_mausoleum.html#af50b878c7740733762c55e80884c5b83',1,'Mausoleum']]],
  ['interactparticles_15',['InteractParticles',['../class_mausoleum.html#a3eef350d15af50c98b7f754da382b76f',1,'Mausoleum']]],
  ['introcutscene_16',['IntroCutscene',['../_scene_controller_8cs.html#a3f66fc222e4df894224e2f6a67493231adea3cd19dff65a76adbc091c8f41691f',1,'SceneController.cs']]],
  ['inturptminigame_17',['InturptMiniGame',['../class_game_manager.html#a6ea1e5b686907e9519a957a3870cc404',1,'GameManager']]],
  ['inventory_18',['Inventory',['../class_game_manager.html#aad34417c996852632b41c2e5263cbcaf',1,'GameManager']]],
  ['inventorybag_19',['InventoryBag',['../class_inventory_bag.html',1,'']]],
  ['inventorybag_2ecs_20',['InventoryBag.cs',['../_inventory_bag_8cs.html',1,'']]],
  ['inventoryclosed_21',['InventoryClosed',['../class_u_i_manager.html#a5346c35874dc66ad0dc67cacb490b9ff',1,'UIManager']]],
  ['inventoryopened_22',['InventoryOpened',['../class_u_i_manager.html#a85165f0e5b9a4fc539560a90082b6690',1,'UIManager']]],
  ['inventoryslot_23',['InventorySlot',['../class_inventory_bag.html#a2ef3abb57575d7c7f6114f12ec42cf3a',1,'InventoryBag']]],
  ['invisiblewallprefab_24',['InvisibleWallPrefab',['../class_tutorial_map_gen.html#a87b4f2345d9cefe6e5959a6ad17588dc',1,'TutorialMapGen']]],
  ['isend_25',['IsEnd',['../class_dialogue.html#ae8c95bee410a90a34b078461979ad03e',1,'Dialogue']]],
  ['ishidden_26',['IsHidden',['../class_game_manager.html#a8e5c7aa15c8df6715e4d399cdd1a11d5',1,'GameManager']]],
  ['isinvestigating_27',['IsInvestigating',['../class_guard.html#a83e885e5703157b9913e17ddcd7e5488',1,'Guard']]],
  ['isowned_28',['IsOwned',['../class_item.html#a537a661f0aae8026f326d2cdcff2e9e2',1,'Item']]],
  ['ispaused_29',['IsPaused',['../class_u_i_manager.html#a9eca6f8ecb6e497804603a5a7ec22083',1,'UIManager']]],
  ['iswalking_30',['IsWalking',['../class_guard_flip_script.html#a750da82d73aa960b72e0bdf8a1b609e8',1,'GuardFlipScript.IsWalking()'],['../class_player_flip_manager.html#a1dbdeb4867b7c46b596e7ee84418a647',1,'PlayerFlipManager.IsWalking()']]],
  ['item_31',['Item',['../class_item.html',1,'Item'],['../class_item.html#a3d7a2b0657acf26ea5c4d712957bc8ff',1,'Item.Item()']]],
  ['item_2ecs_32',['Item.cs',['../_item_8cs.html',1,'']]],
  ['itemselctionscript_33',['ItemSelctionScript',['../class_item_selction_script.html',1,'']]],
  ['itemselctionscript_2ecs_34',['ItemSelctionScript.cs',['../_item_selction_script_8cs.html',1,'']]]
];

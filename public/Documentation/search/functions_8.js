var searchData=
[
  ['increasedificulty_0',['IncreaseDificulty',['../class_coffin_cage_mini_game.html#a544da514d1bcdfc6359e31c08933fa90',1,'CoffinCageMiniGame.IncreaseDificulty()'],['../class_mini_game.html#a39ed5137934c956438f25c3c8209bfc8',1,'MiniGame.IncreaseDificulty()']]],
  ['initialize_1',['Initialize',['../class_general_tile.html#aae11cc1b2a40156bd9b56cb738882c32',1,'GeneralTile.Initialize()'],['../class_grave_tile.html#a41e3b02c3c6aa55de8597f9920047d3d',1,'GraveTile.Initialize()']]],
  ['initializetile_2',['InitializeTile',['../class_tile_initialize.html#a526bd6fe252d9012be5419b9014d579c',1,'TileInitialize']]],
  ['initiate_3',['Initiate',['../class_grave_info_refactor.html#a816a13d23f4082fe941aeac8eae19158',1,'GraveInfoRefactor']]],
  ['initiategrave_4',['InitiateGrave',['../class_tile_data.html#ab1cf1c22cdcd9169f1150cb2e3c50a00',1,'TileData']]],
  ['inturptminigame_5',['InturptMiniGame',['../class_game_manager.html#a6ea1e5b686907e9519a957a3870cc404',1,'GameManager']]],
  ['inventoryclosed_6',['InventoryClosed',['../class_u_i_manager.html#a5346c35874dc66ad0dc67cacb490b9ff',1,'UIManager']]],
  ['inventoryopened_7',['InventoryOpened',['../class_u_i_manager.html#a85165f0e5b9a4fc539560a90082b6690',1,'UIManager']]],
  ['isend_8',['IsEnd',['../class_dialogue.html#ae8c95bee410a90a34b078461979ad03e',1,'Dialogue']]],
  ['ishidden_9',['IsHidden',['../class_game_manager.html#a8e5c7aa15c8df6715e4d399cdd1a11d5',1,'GameManager']]],
  ['isowned_10',['IsOwned',['../class_item.html#a537a661f0aae8026f326d2cdcff2e9e2',1,'Item']]],
  ['item_11',['Item',['../class_item.html#a3d7a2b0657acf26ea5c4d712957bc8ff',1,'Item']]]
];

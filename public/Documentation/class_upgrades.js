var class_upgrades =
[
    [ "Upgrades", "class_upgrades.html#a7a007c43d0f8dbda7e084f63ab5d8480", null ],
    [ "BodyBagsUpgrades", "class_upgrades.html#a64c3614a4bc1d06577988b2ff0dad886", null ],
    [ "BoltCutterUpgrades", "class_upgrades.html#a2269593aeeb622bfca94dd5797dc17c1", null ],
    [ "CurrentBodyBag", "class_upgrades.html#ac7578df073f2a2826f6cc05fb4d2370c", null ],
    [ "CurrentBoltCutters", "class_upgrades.html#a43b0a78578516d739b2d60421d57510f", null ],
    [ "CurrentShoes", "class_upgrades.html#a377dce7cf4719294ff2b69ea37fe45ab", null ],
    [ "CurrentShovel", "class_upgrades.html#abc67d61b1673e0ab754beec64c3bd4fa", null ],
    [ "ShoesUpgrades", "class_upgrades.html#a0b5dc1234b266571667b0ae548d0efd8", null ],
    [ "ShovelUpgrades", "class_upgrades.html#a2b0f57ccd980246b88d5f1e8781ec7db", null ]
];
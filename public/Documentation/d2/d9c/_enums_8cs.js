var _enums_8cs =
[
    [ "Direction", "d2/d9c/_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aa", [
      [ "LEFT", "d2/d9c/_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaa684d325a7303f52e64011467ff5c5758", null ],
      [ "UP", "d2/d9c/_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaafbaedde498cdead4f2780217646e9ba1", null ],
      [ "RIGHT", "d2/d9c/_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaa21507b40c80068eda19865706fdc2403", null ],
      [ "DOWN", "d2/d9c/_enums_8cs.html#a224b9163917ac32fc95a60d8c1eec3aaac4e0e4e3118472beeb2ae75827450f1f", null ]
    ] ],
    [ "GraveQuality", "d2/d9c/_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54", [
      [ "LOW", "d2/d9c/_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54a41bc94cbd8eebea13ce0491b2ac11b88", null ],
      [ "MEDIUM", "d2/d9c/_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54ac87f3be66ffc3c0d4249f1c2cc5f3cce", null ],
      [ "HIGH", "d2/d9c/_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54ab89de3b4b81c4facfac906edf29aec8c", null ],
      [ "NULL", "d2/d9c/_enums_8cs.html#a752b5f40cd9a89595521e6cf45ea7e54a6c3e226b4d4795d518ab341b0824ec29", null ]
    ] ],
    [ "GraveyardTier", "d2/d9c/_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25c", [
      [ "LOW", "d2/d9c/_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25ca41bc94cbd8eebea13ce0491b2ac11b88", null ],
      [ "MEDIUM", "d2/d9c/_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25cac87f3be66ffc3c0d4249f1c2cc5f3cce", null ],
      [ "HIGH", "d2/d9c/_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25cab89de3b4b81c4facfac906edf29aec8c", null ],
      [ "TUTORIAL", "d2/d9c/_enums_8cs.html#a61437643b26ee5c5a2a5aa5bd081a25cab8e2e6f28d96c901009d75312887a6da", null ]
    ] ],
    [ "GuardState", "d2/d9c/_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89", [
      [ "PATROL", "d2/d9c/_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89a5383e862b017e18e1087d61e9f2684d9", null ],
      [ "ALERT", "d2/d9c/_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89a320f86f60f25459ba5550e000b2c3929", null ],
      [ "CAUGHT", "d2/d9c/_enums_8cs.html#a70c76ba151a60c1a66887b3e8525bc89af67a3bdb4927fa4ed55d0a24685ac4b3", null ]
    ] ],
    [ "ImageElement", "d2/d9c/_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517", [
      [ "RED", "d2/d9c/_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517aa2d9547b5d3dd9f05984475f7c926da0", null ],
      [ "GREEN", "d2/d9c/_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517a9de0e5dd94e861317e74964bed179fa0", null ],
      [ "BLUE", "d2/d9c/_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517a1b3e1ee9bff86431dea6b181365ba65f", null ],
      [ "ALPHA", "d2/d9c/_enums_8cs.html#a0db6f7e8d91fb1d5786467a161d1d517a002101f8725e5c78d9f30d87f3fa4c87", null ]
    ] ],
    [ "TileType", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1", [
      [ "GRASS", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1aa3030e42cd8f04255711905a9182399f", null ],
      [ "GRAVE", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a0454988b6250b7e13d8e9222e118c127", null ],
      [ "PATH", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a5ffb5f0d0de78321df46fc7c93ca64a3", null ],
      [ "FENCE", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a680f61224afe352a026a616896f47a29", null ],
      [ "BUSH", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a837b47a7cfe2c08329230f73f9e2a241", null ],
      [ "TREE", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1ac0f8e24cf041639c4fc457ebec9490eb", null ],
      [ "MAUSOLEUM", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1aa94abe5bd38a18a47b94c018915022df", null ],
      [ "GRAVELEFT", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a10231ad1a033eca8b0d26bb45bb99df8", null ],
      [ "GRAVERIGHT", "d2/d9c/_enums_8cs.html#ac9e486ec80ccfdb28a4f4837d419c9f1a5c407100388acb8ef7d4a9276715177c", null ]
    ] ]
];
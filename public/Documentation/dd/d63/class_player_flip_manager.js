var class_player_flip_manager =
[
    [ "SetBlendedEulerAngles", "dd/d63/class_player_flip_manager.html#a6ec7e5ceda40d2d4493ecccaab5c31a8", null ],
    [ "AnimationController", "dd/d63/class_player_flip_manager.html#a33a3f7e7b69d96f8d9df0ae32c63bb0f", null ],
    [ "FlipObject", "dd/d63/class_player_flip_manager.html#a07a375f90a43cd1761ec90fc84134ac3", null ],
    [ "HandDepth", "dd/d63/class_player_flip_manager.html#a2843c08bd533fab2ecf08d91eac0cb03", null ],
    [ "HandObjectDepth", "dd/d63/class_player_flip_manager.html#a560835e6a7377df15ebe5aac96881b28", null ],
    [ "HasLantern", "dd/d63/class_player_flip_manager.html#a0dd283c5aa1e78663a80ea4fd8ee2121", null ],
    [ "HasShovel", "dd/d63/class_player_flip_manager.html#a67df45c9abbf5a3cac8b7dc7dae9fa60", null ],
    [ "IsWalking", "dd/d63/class_player_flip_manager.html#a1dbdeb4867b7c46b596e7ee84418a647", null ],
    [ "Particles", "dd/d63/class_player_flip_manager.html#aa3f8431728eae5b1e902f7a374326b64", null ]
];
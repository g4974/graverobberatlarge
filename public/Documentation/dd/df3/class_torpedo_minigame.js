var class_torpedo_minigame =
[
    [ "CheckLose", "dd/df3/class_torpedo_minigame.html#a16d8e919e27fd5a959b884500d90ae43", null ],
    [ "CheckWin", "dd/df3/class_torpedo_minigame.html#a04b4be0f29ce245fa22e043a32fe724c", null ],
    [ "CoffinOpened", "dd/df3/class_torpedo_minigame.html#aeee387b9dabda4aba9ed50bce9094d87", null ],
    [ "DisarmTrap", "dd/df3/class_torpedo_minigame.html#a1762f54d881bbb9942f0ff76d976be2e", null ],
    [ "SetCoffinOpeningDelay", "dd/df3/class_torpedo_minigame.html#ab2cfe4c157bb22a6ee91aa43d49a92d5", null ],
    [ "SetGoalClickedButtons", "dd/df3/class_torpedo_minigame.html#a9de2315e40bc235ee8f1ffc064d29cd3", null ],
    [ "SetTimerStartingNumber", "dd/df3/class_torpedo_minigame.html#adc37a70249ff8d5a0002587bea1ecf3b", null ],
    [ "GoalClickedButtons", "dd/df3/class_torpedo_minigame.html#a304f51e4f0efdacb14d8514be9cc6d5e", null ]
];
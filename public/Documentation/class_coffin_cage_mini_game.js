var class_coffin_cage_mini_game =
[
    [ "CheckWin", "class_coffin_cage_mini_game.html#a902e65a1ebb1e37c9349f4a07705ee2a", null ],
    [ "CutTrap", "class_coffin_cage_mini_game.html#a7382945749a8ed4f7de50b938c64c3f1", null ],
    [ "GetCurrentNumberOfCuts", "class_coffin_cage_mini_game.html#a9e4e08e8ef3dbc6890117d3cdfb9c33f", null ],
    [ "IncreaseDificulty", "class_coffin_cage_mini_game.html#a544da514d1bcdfc6359e31c08933fa90", null ],
    [ "SetCutNumber", "class_coffin_cage_mini_game.html#ad115572b9e240d9c68aad0826e33b6d5", null ],
    [ "ButtonSprite", "class_coffin_cage_mini_game.html#a0e3ea1a7e5f78101b2c1d33565c9527f", null ],
    [ "FakeCursorObj", "class_coffin_cage_mini_game.html#a96cc5f719bc975eb42025f6ee8ff4ecb", null ],
    [ "PossiblePos", "class_coffin_cage_mini_game.html#a82e5baa343821a22c5b0e5f720377215", null ]
];